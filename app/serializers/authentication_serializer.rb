class AuthenticationSerializer < ActiveModel::Serializer
  attributes :access_token, :user

  def user
    {
      id: object.id,
      name: object.name
    }
  end

  def access_token
    generate_access_token(object.id)
  end

  private

  def generate_access_token(user_id)
    JsonWebToken.encode({user_id: user_id})
  end
end