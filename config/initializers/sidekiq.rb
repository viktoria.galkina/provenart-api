# frozen_string_literal: true

require 'sidekiq/web'
require 'sidekiq-scheduler/web'

redis_con = {
  url: ENV['REDIS_URL']
}

Sidekiq.configure_server do |config|
  config.redis = redis_con
end

Sidekiq.configure_client do |config|
  config.redis = redis_con
end

Sidekiq::Web.set :session_secret, Rails.application.secrets[:secret_key_base]

unless Rails.env.development? || Rails.env.test?
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username_params = ::Digest::SHA256.hexdigest(username), ::Digest::SHA256.hexdigest(ENV['SIDEKIQ_USERNAME'])
    password_params = ::Digest::SHA256.hexdigest(password), ::Digest::SHA256.hexdigest(ENV['SIDEKIQ_PASSWORD'])

    username_secure_compare = ActiveSupport::SecurityUtils.secure_compare(username_params[0], username_params[1])
    password_secure_compare = ActiveSupport::SecurityUtils.secure_compare(password_params[0], password_params[1])

    # - Use & (do not use &&) so that it doesn't short circuit.
    username_secure_compare & password_secure_compare
  end
end

Sidekiq::Extensions.enable_delay!
