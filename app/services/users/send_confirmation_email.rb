# frozen_string_literal: true

module Services
  module Users
    class SendConfirmationEmail < BaseService
      def initialize
        @adapter = Adapters::MailjetAdapter.new
      end

      def call(user_id)
        user = User.find_by(id: user_id)
        templates = Settings.mailjet
        return unless user&.email

        options = {
          to: user.email,
          name: user.name,
          template_id: templates.confirmation_email_template,
          variables: {
            name: user.name,
            url: generate_confirmation_url(user.id)
          }
        }

        adapter.send_email options
      end

      private

      attr_reader :adapter

      def generate_confirmation_url(user_id)
        URI::HTTPS.build(
          host: ENV.fetch('API_HOST'),
          path: '/confirm-email',
          query: "key=#{confirmation_key(user_id)}"
        ).to_s
      end

      def confirmation_key(user_id)
        expire_at = 1.day.from_now.to_i

        payload = { user_id: user_id, exp: expire_at }

        JsonWebToken.encode(payload)
      end
    end
  end
end
