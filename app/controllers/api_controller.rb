class ApiController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :handle_not_found

  before_action :authenticate

  def current_user
    @current_user
  end

  private

  def authenticate
    set_current_user || handle_bad_authentication
  end

  def set_current_user
    return unless request.headers['Authorization']

    @current_user = User.find_by(id: decoded_auth_token[:user_id]) if decoded_auth_token
  rescue JWT::ExpiredSignature, JWT::DecodeError
    nil
  end

  def auth_header_token
    request.headers['Authorization'].split(' ').last
  end

  def decoded_auth_token
    JsonWebToken.decode(auth_header_token)
  end

  def handle_bad_authentication
    render json: { message: "Bad credentials" }, status: :unauthorized
  end

  def handle_not_found
    render json: { message: "Record not found" }, status: :not_found
  end
end
