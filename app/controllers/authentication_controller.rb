class AuthenticationController < ApplicationController

  # POST /users
  def sign_up
    user = User.new(user_params)

    if user.save
      Services::Users::SendConfirmationEmail.delay.call(user.id)
      render json: {message: 'Account created. Confirm your email'}, status: :created
    else
      render json: user.errors, status: :unprocessable_entity
    end
  end

  def sign_in
    user = User.find_by(email: params[:user][:email].downcase)

    not_existing_user and return unless user
    password_invalid and return if user.password_digest.blank? || !user.authenticate(params[:user][:password])
    email_not_confirmed and return unless user.confirmed?

    render json: user, status: :created, serializer: AuthenticationSerializer
  end

  def confirm_email
    update_user!
    redirect_to 'https://provenart.com/email_confirm.html', status: 301
  end

  private
    # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(:name, :email, :password)
  end

  def update_user!
    user = User.find_by(id: JsonWebToken.decode(params[:key])[:user_id])
    return unless user

    user.update confirmed_at: DateTime.current
  end

  def not_existing_user
    render json: { message: 'Email address could not be found' }, status: :forbidden
  end

  def password_invalid
    render json: { message: 'Password invalid' }, status: :forbidden
  end

  def email_not_confirmed
    render json: { message: 'Email not confirmed. Code sent to your email box' }, status: :forbidden
  end
end
