class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    enable_extension 'citext'

    create_table :users do |t|
      t.string :name, default: '', null: false, index: true
      t.citext :email, null: false
      t.text :password_digest, default: '', null: false
      t.datetime :confirmed_at

      t.timestamps
    end

    add_index :users, :email, unique: true
  end
end
