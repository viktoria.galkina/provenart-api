class User < ApplicationRecord
  has_secure_password validations: false

  PASSWORD_REQUIREMENTS = /\A
    (?=.{8,}) # At least 8 char long
  /x.freeze

  validates :name, :email, presence: true
  validates :password, format: {
    with: PASSWORD_REQUIREMENTS,
    message: 'Password must be at least 8 characters long'
  }, unless: -> { password.nil? }

  def confirm!
    update_columns(confirmed_at: Time.current)
  end

  def confirmed?
    confirmed_at.present?
  end
end
