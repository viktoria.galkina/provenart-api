Rails.application.routes.draw do
  post 'sign_up', to: 'authentication#sign_up'
  post 'sign_in', to: 'authentication#sign_in'
  get 'confirm-email', to: 'authentication#confirm_email'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
